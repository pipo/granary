The granary
===========

The granary is where some of the ingredients are kept.

This repository contains scripts and code for many things. 

mkinitramfs
-----------

This script is responsible for generating the initramfs.

The initramfs is a file that lies alongside the kernel in the /boot folder. It
represents a filesystem and files on it, and is loaded in the main memory (hence
the name initRAMfs) and is considered as the root partition by the kernel.
It runs the init script from this filesystem, which loads many things and
switches the root file system to the actual one.

In the Pipo Distribution, we do not have packages installed on the root
filesystem, so we need an initramfs. This initramfs is made of modules which may
come from different packages. It is configured with the /etc/initrd.conf file.
Its main purpose is to load the packages onto the main filesystem.

The basic configuration is to have "base" as the only module. If you need
another module, you need to specify it in the configuration file. Be careful
that the order in the configuration is meaningful. You should check the Pipo
Distribution's documentation before editing the configuration, as this may
render your system unbootable.

In case your system does not boot anymore after a run of mkinitramfs, you can
always go back to a previous version of the initramfs. They are kept as
/boot/initrd.*timestamp_of_the_backup*.